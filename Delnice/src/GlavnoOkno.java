import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


@SuppressWarnings("serial")
public class GlavnoOkno extends JFrame implements ActionListener{
	private JButton gumb;
	private Izrisuj graf;
	private IzpisDatuma datum;
	private IzpisCene cena;
	public static Vector<Double> cenaDelnice;
	static String IzbranaDelnica;
	
	
	
	public GlavnoOkno() throws IOException{
		super();
		setTitle("Gibanje cen delnic");
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		// Dodajanje imena komponent
		c.gridx = 0;	
		c.gridy = 0;
		add(new JLabel("Ime delnice"),c);
		
		c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 0;
		c.gridwidth = GridBagConstraints.REMAINDER;
		add(new JLabel("Graf"),c);
		
		c = new GridBagConstraints();
		c.gridx = 4;
		c.gridy = 3;
		c.gridheight = 3;
		c.weightx = 1;
		c.weighty = 1;
		cena = new IzpisCene();
		add(cena,c);
		
		c = new GridBagConstraints();
		c.gridx = 4;
		c.gridy = 6;
		c.gridheight = 2;
		c.weightx = 1;
		c.weighty = 1;
		datum = new IzpisDatuma();
		add(datum,c);
		

		
		// Dodajanje gumbov za izbiro celnice, ki se izri�e
		BufferedReader vhod = new BufferedReader(new FileReader("./delnice.csv"));
		String vrstica[] =  vhod.readLine().trim().split(";");
		JPanel buttonPanel = new JPanel(new GridLayout(8, 1));
		for (int i=1; i<=8; i++){
			c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy=i;
			gumb = new JButton(vrstica[i].replaceAll("\"", ""));
			gumb.setSize(200,10);
			gumb.addActionListener(this);
			buttonPanel.add(gumb,c);
		}
		vhod.close();
		
		c.anchor = GridBagConstraints.NORTH;
        c.weighty = 1;
        c.gridx = 0;
		c.gridy=2;
        add(buttonPanel, c);
		
		// Dodajanje polja za risanje grafa
		c = new GridBagConstraints();
		c.gridx = 2;
		c.gridy = 2;
		c.gridwidth = 2;
		c.gridheight = 8;
		graf = new Izrisuj();
		graf.addMouseMotionListener(new CustomMouseMotionListener());
		add(graf,c);
	}


	@Override
	public void actionPerformed(ActionEvent arg0) {
//		izbrano delnico shranim kot spremenljivko tipa string
		IzbranaDelnica = ((JButton)(arg0.getSource())).getText();
		
//		ustvarim prazen vektor
		cenaDelnice = new Vector<Double>();
		if (IzbranaDelnica.equals("Goldman Sachs")){ // �e je izbrana delnica Nike, pokli�em vektor cen te delnice
			cenaDelnice = BranjePodatkov.GoldmanSach;
		}
		else if (IzbranaDelnica.equals("Apple")){
			cenaDelnice = BranjePodatkov.Apple;
		}
		else if (IzbranaDelnica.equals("Microsoft")){
			cenaDelnice = BranjePodatkov.Microsoft;
		}
		else if (IzbranaDelnica.equals("Nike")){
			cenaDelnice = BranjePodatkov.Nike;
		}
		else if (IzbranaDelnica.equals("Boeing")){
			cenaDelnice = BranjePodatkov.Boeing;
		}
		else if (IzbranaDelnica.equals("Coca-Cola")){
			cenaDelnice = BranjePodatkov.CocaCola;
		}
		else if (IzbranaDelnica.equals("JPMorgan")){
			cenaDelnice = BranjePodatkov.JPMorgan;
		}
		else if (IzbranaDelnica.equals("Intel")){
			cenaDelnice = BranjePodatkov.Intel;
		}
		else{
			cenaDelnice = null;

		}
		graf.setGraf(cenaDelnice);

	}
	
	
	
	class CustomMouseMotionListener implements MouseMotionListener {
		public void mouseMoved(MouseEvent arg0) {
			int x = arg0.getX();
			datum.setDatum(x);
			cena.setCena(x);
			
	      }

		@Override
		public void mouseDragged(MouseEvent arg0) {
			// TODO Auto-generated method stub
//			te metode ne potrebujem
			
		}    
	   }
}
