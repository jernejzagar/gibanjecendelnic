import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.util.Vector;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Izrisuj extends JPanel {
	
	private Vector<Double> cena;

	public Izrisuj(){
		super();
	}
	
	public void setGraf(Vector<Double> cenaDelnice) { 
		this.cena = cenaDelnice;
		repaint();

	}
	
	@Override
    public Dimension getPreferredSize() {
        return new Dimension(400, 300);
    }
	
	@Override
	protected void paintComponent(Graphics gg) {
		super.paintComponent(gg);
		Graphics2D g = (Graphics2D) gg;
		setBackground(Color.PINK);
		
		if (cena==null){
			gg.setColor(Color.RED);
			Font font = new Font("Serif", Font.BOLD, 20);
			g.setFont(font);
		    g.drawString("Izberi delnico",130,150);
		}
		
		else{
			g.drawLine(0,300-1,400,300-1);
			g.drawLine(1,0,1,400);
			g.drawString("�as",370,290);
			g.drawString("Cena" ,10,20);
			Font font = new Font("Serif", Font.BOLD, 20);
			g.setFont(font);
			g.setColor(Color.BLUE);
			g.drawString(GlavnoOkno.IzbranaDelnica,175,20);
			
			gg.setColor(Color.BLUE);
			for (int i=0;i<cena.size();i++){
				Ellipse2D.Double shape = new Ellipse2D.Double(i+2,300-cena.get(i), 3, 3);
			    g.draw(shape);
			}
		}
	}	
}