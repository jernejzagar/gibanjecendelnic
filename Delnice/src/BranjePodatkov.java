import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;


public class BranjePodatkov {
		
		public static Vector<String> Datumi = new Vector<String>();
		public static Vector<Double> GoldmanSach = new Vector<Double>();
		public static Vector<Double> Apple = new Vector<Double>();
		public static Vector<Double> Microsoft = new Vector<Double>();
		public static Vector<Double> Nike = new Vector<Double>();
		public static Vector<Double> Boeing = new Vector<Double>();
		public static Vector<Double> CocaCola = new Vector<Double>();
		public static Vector<Double> JPMorgan = new Vector<Double>();
		public static Vector<Double> Intel = new Vector<Double>();
		
		public static void preberi() throws IOException {
			
			BufferedReader vhod = new BufferedReader(new FileReader("./delnice.csv"));
			vhod.readLine();
			String line1=null;
			while ((line1 = vhod.readLine()) != null){
				String  Datum = line1.trim().split(";")[0];
				double GoldmanSach1 = Double.parseDouble(line1.trim().split(";")[1].replaceAll(",", "."));
				double Apple1 = Double.parseDouble(line1.trim().split(";")[2].replaceAll(",", "."));
				double Microsoft1 = Double.parseDouble(line1.trim().split(";")[3].replaceAll(",", "."));
				double Nike1 = Double.parseDouble(line1.trim().split(";")[4].replaceAll(",", "."));
				double Boeing1 = Double.parseDouble(line1.trim().split(";")[5].replaceAll(",", "."));
				double CocaCola1 = Double.parseDouble(line1.trim().split(";")[6].replaceAll(",", "."));
				double JPMorgan1 = Double.parseDouble(line1.trim().split(";")[7].replaceAll(",", "."));
				double Intel1 = Double.parseDouble(line1.trim().split(";")[8].replaceAll(",", "."));
				Datumi.add(Datum);
				GoldmanSach.add(GoldmanSach1);
				Apple.add(Apple1);
				Microsoft.add(Microsoft1);
				Nike.add(Nike1);
				Boeing.add(Boeing1);
				CocaCola.add(CocaCola1);
				JPMorgan.add(JPMorgan1);
				Intel.add(Intel1);
			}
		
			vhod.close();
		}
}
