import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;


@SuppressWarnings("serial")
public class IzpisCene extends JPanel {
	
	private Double cena;
	
	public IzpisCene(){
		super();
	}
	
	public void setCena(int x) {
		if (GlavnoOkno.IzbranaDelnica!=null && x>0 && x<BranjePodatkov.Datumi.size()){
		this.cena = GlavnoOkno.cenaDelnice.get(x);
		}
		repaint();
	}

	
	@Override
    public Dimension getPreferredSize() {
        return new Dimension(100, 50);
    }
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		setBackground(Color.GREEN);
		if (cena==null){
			g.drawString("Cena", 0, 15);
		}
		else{
			String s = String.valueOf(cena);
			g.drawString(GlavnoOkno.IzbranaDelnica, 0, 15);
			g.drawString("Cena delnice:",0,35);
			g.drawString(s, 0, 50);
		}
	}

}