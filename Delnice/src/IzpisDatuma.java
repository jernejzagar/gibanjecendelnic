import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;


@SuppressWarnings("serial")
public class IzpisDatuma extends JPanel{
	
	private String datum;
	
	public IzpisDatuma(){
		super();
	}
	
	public void setDatum(int x) {
		if (x>0 && x<BranjePodatkov.Datumi.size()){
			this.datum = BranjePodatkov.Datumi.get(x);
		}
		repaint();
	}
	
	@Override
    public Dimension getPreferredSize() {
        return new Dimension(100, 50);
    }
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		setBackground(Color.GREEN);
		if (GlavnoOkno.IzbranaDelnica==null){
			g.drawString("Datum", 0, 15);
		}
		else{
			g.drawString("Datum:",0,30);
			g.drawString(datum.replaceAll("\"", ""), 0, 45);
		}
	}

}